import Home from "../pages/Home";
import Popular from "../pages/Popular";
import Battle from "../pages/Battle";
import Results from "../pages/Results";
import {ReactElement} from "react";
export interface IRoute {
    path: string
    name?: string
    element: ReactElement
}
export const routes:Array<IRoute> = [
    {
        path: '/',
        name: 'Home',
        element: <Home />
    },
    {
        path: 'popular',
        name: 'Popular',
        element: <Popular />
    },
    {
        path: 'battle',
        name: 'Battle',
        element: <Battle />
    },
    {
        path: 'battle/results',
        element: <Results />
    },
]
