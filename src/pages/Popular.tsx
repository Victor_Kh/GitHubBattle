import {FC, ReactElement} from "react";
import Tabs from "../components/Tabs";
import RepositoryList from "../components/RepositoryList";

const Popular:FC = ():ReactElement => {
    return (
        <>
            <Tabs />
            <RepositoryList />
        </>
    );
};

export default Popular;
