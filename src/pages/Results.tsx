import React, {FC, ReactElement, useEffect} from 'react';
import {useSearchParams} from "react-router-dom";
import PlayerPreview from "../components/PlayerPreview";
import PlayerPreviewDetails from "../components/PlayerPreviewDetails";
import Loader from "../components/ui/Loader";
import Error from "../components/ui/Error";
import {useDispatch, useSelector} from "react-redux";
import { resetBattleResult} from "../store/battleSlice";
import {resetPlayersForm} from "../store/playersDataSlice";
import {makeBattleRequest} from "../store/thunkActions";
import {AppDispatch, RootState} from "../store/store";
import {IBattleState} from "../types/battleTypes";
const Results:FC = ():ReactElement => {
    const [searchParams] = useSearchParams()
    const playerOneLogin = searchParams.get('playerOneName')
    const playerTwoLogin = searchParams.get('playerTwoName')
    const dispatch:AppDispatch = useDispatch()
    const {isLoading, error, winner, loser}:IBattleState = useSelector((state: RootState):IBattleState => state.battleSlice)

    useEffect(() => {
        dispatch(makeBattleRequest([playerOneLogin as string, playerTwoLogin as string]))
        return () => {
            dispatch(resetPlayersForm('playerOne'))
            dispatch(resetPlayersForm('playerTwo'))
            dispatch(resetBattleResult(null))
        }
    },[])

    if (isLoading) {
        return <Loader />
    }
    if (error) {
        return <Error />
    }
    return (
        <div className='row'>
            <PlayerPreview playerName={winner.userData.login} playerImage={winner.userData.avatar_url}>
                <PlayerPreviewDetails playerData={winner}/>
            </PlayerPreview>
            <PlayerPreview playerName={loser.userData.login} playerImage={loser.userData.avatar_url}>
                <PlayerPreviewDetails playerData={loser}/>
            </PlayerPreview>
        </div>
    );
};

export default Results;
