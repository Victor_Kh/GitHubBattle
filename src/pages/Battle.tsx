import PlayerInput from "../components/PlayerInput";
import PlayerPreview from "../components/PlayerPreview";
import {Link} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {resetPlayersForm} from "../store/playersDataSlice";
import {AppDispatch, RootState} from "../store/store";
import {IPlayerDataState} from "../types/playerDataTypes";
import {FC, ReactElement} from "react";

const Battle:FC = ():ReactElement => {
    const dispatch:AppDispatch = useDispatch()
    const { playerOneName, playerOneImage, playerTwoImage, playerTwoName }:IPlayerDataState = useSelector((state:RootState):IPlayerDataState => state.playersDataSlice)
    return (
        <>
            <div className='row'>
                {playerOneImage ?
                    <PlayerPreview playerName={playerOneName} playerImage={playerOneImage}>
                        <button className='reset' onClick={() => dispatch(resetPlayersForm('playerOne'))}>Reset</button>
                    </PlayerPreview> :
                    <PlayerInput label={'Player 1'}
                                 id='playerOne' />}
                {playerTwoImage ?
                    <PlayerPreview playerName={playerTwoName} playerImage={playerTwoImage}>
                        <button className='reset' onClick={() => dispatch(resetPlayersForm('playerTwo'))}>Reset</button>
                    </PlayerPreview>:
                    <PlayerInput label={'Player 2'}
                                 id='playerTwo' />}
            </div>
            {playerOneImage && playerTwoImage ?
                <Link to={{
                    pathname: 'results',
                    search: `?playerOneName=${playerOneName}&playerTwoName=${playerTwoName}`
                }} className='button'>Battle</Link>:
                null
            }
        </>
    );
};

export default Battle;
