import React, {FC, ReactElement} from "react";
import {RouterProvider} from "react-router-dom";
import {router} from "./routing/router";
const App:FC = ():ReactElement => {
  return (
      <RouterProvider router={router} />
  );
};

export default App;
