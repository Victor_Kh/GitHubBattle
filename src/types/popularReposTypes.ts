export interface IRepo {
    [key: string]: any
    id:string
    name: string
    owner: {
        avatar_url: string
        login: string
    }
    html_url: string,
    stargazers_count: number
}
export type TPopularRepo = [] | IRepo[]
export interface IPopularState {
    selectedLanguage: string,
    isLoading: boolean,
    popularRepos: TPopularRepo,
    error: null | string
}
