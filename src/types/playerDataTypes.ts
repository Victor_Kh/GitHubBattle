export interface IPlayerDataState {
    [key: string]: string
    playerOneName: string,
    playerTwoName: string,
    playerOneImage: string,
    playerTwoImage: string,
}
