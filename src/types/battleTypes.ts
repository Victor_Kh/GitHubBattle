export interface IUserData {
    [key: string]: string | number | boolean | null
    blog: string
    login: string
    avatar_url: string
    followers: number
}

export interface IUserWithScore {
    userData: IUserData,
    score: number
}
export interface IBattleState {
    isLoading: boolean,
    loser: IUserWithScore,
    winner: IUserWithScore,
    error: null | string
}
