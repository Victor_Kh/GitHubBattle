import {IRepo} from "../types/popularReposTypes";
import {IUserData, IUserWithScore} from "../types/battleTypes";

const getStarRepo = (repos: Array<IRepo>):number => {
  return repos.reduce((acc:number, current:IRepo) => {
      return acc + current.stargazers_count
  },0)
}

export const calculateScore = (userData: IUserData, repos: Array<IRepo>) => {
    const { followers } = userData
    const totalStars = getStarRepo(repos)

    return followers + totalStars
}

export const sortPlayers = (players: Array<IUserWithScore>) => {
    return players.sort((a, b) => b.score - a.score)
}
