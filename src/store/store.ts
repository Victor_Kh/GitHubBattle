import { configureStore } from "@reduxjs/toolkit";
import logger from "redux-logger";
import popularSlice from "./popularSlice";
import playersDataSlice from "./playersDataSlice";
import battleSlice from "./battleSlice";

export const store = configureStore({
    reducer: {
        popularSlice,
        battleSlice,
        playersDataSlice
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger)
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
