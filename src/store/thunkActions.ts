import {createAsyncThunk} from "@reduxjs/toolkit";
import {fetchPopularRepos, makeBattle} from "../api/api";
import {selectLanguage} from "./popularSlice";

export const getPopularRepos = createAsyncThunk(
    'popular/getPopularRepos',
    async (selectedLanguage: string, { rejectWithValue, dispatch })=> {
        dispatch(selectLanguage(selectedLanguage))
        try {
            const response = await fetchPopularRepos(selectedLanguage)
            return response.data.items
        }
        catch (e) {
            return rejectWithValue(e)
        }
    }
)

export const makeBattleRequest = createAsyncThunk(
    'battle/makeBattle',
    async (players: Array<string>, { rejectWithValue }) => {
        try {
            return await makeBattle(players)
        }
        catch (e) {
            return rejectWithValue(e)
        }
    }
)
