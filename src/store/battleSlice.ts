import {ActionReducerMapBuilder, AnyAction, createSlice, PayloadAction, Slice} from "@reduxjs/toolkit";
import {makeBattleRequest} from "./thunkActions";
import {IBattleState, IUserWithScore} from "../types/battleTypes";

const initialState :IBattleState = {
    isLoading: false,
    loser: {
        userData: {
            blog: '',
            login: '',
            avatar_url: '',
            followers: 0
        },
        score: 0
    },
    winner: {
        userData: {
            blog:'',
            login: '',
            avatar_url: '',
            followers: 0
        },
        score: 0
    },
    error: null
}
const battleSlice: Slice<IBattleState> = createSlice({
    name: 'battle',
    initialState,
    reducers: {
        resetBattleResult(state: IBattleState): void {
            state.winner = {userData: {blog:'',login:'', avatar_url:'',followers: 0}, score: 0}
            state.loser = {userData: {blog:'',login:'', avatar_url:'',followers: 0}, score: 0}
        }
    },
    extraReducers: (builder:ActionReducerMapBuilder<IBattleState>) => {
        builder.addCase(makeBattleRequest.pending, (state:IBattleState): void => {
            state.isLoading = true
            state.error = null
        })
        builder.addCase(makeBattleRequest.fulfilled, (state: IBattleState, { payload }: PayloadAction<Array<IUserWithScore>>): void => {
            const [winner, loser]:Array<IUserWithScore> = payload
            state.isLoading = false
            state.loser = loser
            state.winner = winner
        })
        builder.addCase(makeBattleRequest.rejected, (state: IBattleState, { payload }: AnyAction): void => {
            state.isLoading = false
            state.error = payload
        })
    }
})

export const { resetBattleResult } = battleSlice.actions

export default battleSlice.reducer
