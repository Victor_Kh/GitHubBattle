import {createSlice, PayloadAction, Slice} from "@reduxjs/toolkit";
import {IPlayerDataState} from "../types/playerDataTypes";

const initialState :IPlayerDataState = {
    playerOneName: '',
    playerTwoName: '',
    playerOneImage: '',
    playerTwoImage: '',
}

const playerDataSlice: Slice<IPlayerDataState> = createSlice({
    name: 'playersData',
    initialState,
    reducers: {
        submitPlayerFormAction(state: IPlayerDataState, { payload }: PayloadAction<{id:string, userName:string}>) :void {
            state[`${payload.id}Name`] = payload.userName
            state[`${payload.id}Image`] = `https://github.com/${payload.userName}.png?size200`
        },
        resetPlayersForm(state, { payload }: PayloadAction<{id:string}>) :void {
            state[`${payload}Name`] = ''
            state[`${payload}Image`] = ''
        }
    }
})

export const { submitPlayerFormAction, resetPlayersForm } = playerDataSlice.actions

export default playerDataSlice.reducer
