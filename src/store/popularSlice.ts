import {ActionReducerMapBuilder, AnyAction, createSlice, PayloadAction} from "@reduxjs/toolkit";
import {getPopularRepos} from "./thunkActions";
import {Slice} from "@reduxjs/toolkit";
import {IPopularState, TPopularRepo} from "../types/popularReposTypes";

const initialState:IPopularState = {
    selectedLanguage: 'All',
    isLoading: false,
    popularRepos: [],
    error: null
}

const popularSlice:Slice<IPopularState> = createSlice({
    name: 'popular',
    initialState,
    reducers: {
        selectLanguage(state :IPopularState, { payload }: PayloadAction<string>) :void {
            state.selectedLanguage = payload
        }
    },
    extraReducers: (builder :ActionReducerMapBuilder<IPopularState>) :void => {
        builder.addCase(getPopularRepos.pending, (state :IPopularState) :void => {
            state.isLoading = true
            state.error = null
        })
        builder.addCase(getPopularRepos.fulfilled, (state :IPopularState, { payload } :PayloadAction<TPopularRepo>) :void => {
            state.isLoading = false
            state.popularRepos = payload
        })
        builder.addCase(getPopularRepos.rejected, (state :IPopularState, { payload } :AnyAction) :void => {
            state.isLoading = false
            state.error = payload
        })
    }
})

export const { selectLanguage} = popularSlice.actions

export default popularSlice.reducer
