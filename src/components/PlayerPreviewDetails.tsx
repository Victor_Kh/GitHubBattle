import React, {FC, ReactElement} from 'react';
import {Link} from "react-router-dom";
import {IUserWithScore} from "../types/battleTypes";

interface IPlayerPreviewDetailsProps {
    playerData: IUserWithScore
}
const PlayerPreviewDetails:FC<IPlayerPreviewDetailsProps> = ({playerData}:IPlayerPreviewDetailsProps):ReactElement => {
    const {name, login, location, company, followers, following, public_repos, blog} = playerData.userData
    return (
        <div>
            <div>
                <span>Score </span>
                <span>{playerData.score}</span>
            </div>
            <div>
                <span>Name: </span>
                <span>{name || login}</span>
            </div>
            <div>
                <span>Location: </span>
                <span>{location || '-'}</span>
            </div>
            <div>
                <span>Company: </span>
                <span>{company || '-'}</span>
            </div>
            <div>
                <span>Followers: </span>
                <span>{followers}</span>
            </div>
            <div>
                <span>Following: </span>
                <span>{following}</span>
            </div>
            <div>
                <span>Public Repos: </span>
                <span>{public_repos}</span>
            </div>
            <div>
                <span>Blog: </span>
                <Link to={blog} target='_blank'>{blog || '-'}</Link>
            </div>
        </div>
    );
};

export default PlayerPreviewDetails;
