import React, {FC, ReactElement} from 'react';
import {IRepo} from "../types/popularReposTypes";
interface IRepositoryListItemProps {
    repository: IRepo
    idx: number
}
const RepositoryListItem:FC<IRepositoryListItemProps> = ({repository, idx}:IRepositoryListItemProps):ReactElement => {
    return (
        <li className='popular-item'>
            <div className='popular-rank'>#{idx+1}</div>
            <ul className='space-list-items'>
                <li>
                    <img className='avatar' src={repository.owner.avatar_url} alt="avatar"/>
                </li>
                <li>
                    <a href={repository.html_url} target='_blank' rel='noreferrer'>{repository.name}</a>
                </li>
                <li>
                    @{repository.owner.login}
                </li>
                <li>
                    <>{repository.stargazers_count} stars</>
                </li>
            </ul>
        </li>
    );
};

export default RepositoryListItem;
