import React, {memo, FC, ReactElement} from 'react';
interface IPlayerPreviewProps {
    playerImage: string;
    playerName: string;
    children: ReactElement
}
const PlayerPreview:FC<IPlayerPreviewProps> = memo(({ playerImage, playerName, children }:IPlayerPreviewProps):ReactElement => {
    return (
        <div className='column'>
            <img className='avatar' src={playerImage} alt="Avatar"/>
            <h3>{playerName}</h3>
            {children}
        </div>
    );
});

export default PlayerPreview;
