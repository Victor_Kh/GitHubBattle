import {useDispatch, useSelector} from "react-redux";
import {FC, ReactElement, useEffect} from "react";
import {getPopularRepos} from "../store/thunkActions";
import {AppDispatch, RootState} from "../store/store";

const languages:string[] = ['All', 'Javascript', 'Java', 'Python', 'CSS', 'Ruby']

const Tabs:FC = ():ReactElement => {
    const requestedLanguage:string = useSelector((state:RootState):string => state.popularSlice.selectedLanguage)
    const dispatch:AppDispatch = useDispatch()

    useEffect((): void => {
        dispatch(getPopularRepos(requestedLanguage))
    },[])

    return (
        <ul className='languages'>
            {languages.map((language:string, idx:number):ReactElement => {
                return (
                    <li key={idx}
                        style={{color: language === requestedLanguage ? '#d0021b': '#000'}}
                        onClick={() => dispatch(getPopularRepos(language))}>
                        {language}
                    </li>
                )
            })}
        </ul>
    );
};

export default Tabs;
