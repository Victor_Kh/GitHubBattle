import React, {ChangeEvent, FC, FormEvent, memo, ReactElement, useState} from 'react';
import {submitPlayerFormAction} from "../store/playersDataSlice";
import {useDispatch} from "react-redux";
import {AppDispatch} from "../store/store";

interface IPlayerInputProps {
    id: string,
    label:string
}

const PlayerInput:FC<IPlayerInputProps> = memo(({ id, label }:IPlayerInputProps):ReactElement => {
    const [userName, setUserName] = useState<string>('')
    const dispatch:AppDispatch = useDispatch()
    const handleSubmit = (e:FormEvent<HTMLFormElement>):void => {
        e.preventDefault()
        dispatch(submitPlayerFormAction({id, userName}))
    }
    return (
        <form className='column' onSubmit={handleSubmit}>
            <label htmlFor={id}>{label}</label>
            <input id={id}
                   type="text"
                   value={userName}
                   autoComplete='off'
                   placeholder='GitHub Username'
                   onChange={(e:ChangeEvent<HTMLInputElement>) => setUserName(e.target.value)}/>
            <button className='button' type='submit'>
                Submit
            </button>
        </form>
    );
});

export default PlayerInput;
