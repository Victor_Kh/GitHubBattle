import React, {FC, ReactElement} from 'react';
import RepositoryListItem from "./RepositoryListItem";
import Loader from "./ui/Loader";
import {useSelector} from "react-redux";
import Error from "./ui/Error";
import {IPopularState, IRepo} from "../types/popularReposTypes";
import {RootState} from "../store/store";

const RepositoryList:FC = ():ReactElement => {
    const {popularRepos, isLoading, error}:IPopularState = useSelector((state:RootState):IPopularState => state.popularSlice)

    if (isLoading) {
        return  <Loader />
    }
    if (error) {
        return <Error />
    }
    return (
        <div className='repository-list-wrapper'>
            <ul className='popular-list'>
                {popularRepos.map((repository:IRepo, idx:number) => <RepositoryListItem repository={repository}
                                                                           idx={idx}
                                                                           key={repository.id}/>)}
            </ul>
        </div>
    );
};

export default RepositoryList;
