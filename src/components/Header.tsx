import React, {FC, ReactElement} from 'react';
import Navigation from "./Navigation";

const Header:FC = ():ReactElement => {
    return (
        <header>
            <Navigation />
        </header>
    );
};

export default Header;
