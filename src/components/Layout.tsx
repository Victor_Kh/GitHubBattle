import React, {FC, ReactElement} from 'react';
import Header from "./Header";
import {Outlet} from "react-router-dom";

const Layout:FC = ():ReactElement => {
    return (
        <div className='container'>
            <Header />
            <Outlet />
        </div>
    );
};

export default Layout;
