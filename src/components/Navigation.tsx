import React, {FC, ReactElement} from 'react';
import {IRoute, routes} from "../routing/routes";
import {NavLink} from "react-router-dom";

const Navigation:FC = ():ReactElement => {
    return (
        <ul className='nav'>
            {routes.map((route:IRoute, idx:number) => {
                if (route.name) {
                    return (
                        <li key={idx}>
                            <NavLink to={route.path}>{route.name}</NavLink>
                        </li>
                    )
                }
            })}
        </ul>
    );
};

export default Navigation;
