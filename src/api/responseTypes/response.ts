import {IRepo} from "../../types/popularReposTypes";

export interface IPopularReposResponse {
    items: Array<IRepo>
}
