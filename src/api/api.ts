import axios, {AxiosResponse} from "axios";
import {calculateScore, sortPlayers} from "../utils/utils";
import {IPopularReposResponse} from "./responseTypes/response";
import {IUserData, IUserWithScore} from "../types/battleTypes";
import {IRepo} from "../types/popularReposTypes";

export const fetchPopularRepos = (language: string):Promise<AxiosResponse<IPopularReposResponse>> => {
    return axios.get<IPopularReposResponse>(window.encodeURI(`https://api.github.com/search/repositories?q=stars:>1+language:${language}&sort=desc&type=Repositories`))
}

const fetchUserData = async (username: string):Promise<IUserData> => {
     try {
         const response:AxiosResponse<IUserData> = await axios.get<IUserData>(window.encodeURI(`https://api.github.com/users/${username}`))
         return response.data
     }
     catch (e:any) {
         throw new Error(e)
     }
}

const fetchUserRepos = async (username:string):Promise<Array<IRepo>> => {
    try {
        const response:AxiosResponse<Array<IRepo>> = await axios.get<Array<IRepo>>(window.encodeURI(`https://api.github.com/users/${username}/repos`))
        return response.data
    }
    catch (e:any) {
        throw new Error(e)
    }
}

const getUserData = async (username:string):Promise<IUserWithScore> => {
    try {
        const response:[IUserData, Array<IRepo>] = await Promise.all([fetchUserData(username), fetchUserRepos(username)])
        const [userData, repos]: [IUserData, Array<IRepo>] = response
        return {
            userData,
            score: calculateScore(userData, repos)
        }
    }
    catch (e:any) {
        throw new Error(e)
    }
}

export const makeBattle = async (players:Array<string>):Promise<Array<IUserWithScore>> => {
    try {
        const response:Array<IUserWithScore> = await Promise.all(players.map((player:string) => getUserData(player)))
        return sortPlayers(response)
    }
    catch (e:any) {
        throw new Error(e)
    }
}
